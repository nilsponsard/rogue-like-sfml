#include <cstdlib>
#include <ctime>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <vector>

#include "snake/Snake.h"
#include "apple/Apple.h"
#include "constants.h"

int main() {

    sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");

    snakeNameSpace::Snake currentSnake(sf::Vector2f(200, 200));
/*  float x (0);
    float y (0);
    */

    snakeNameSpace::Apple currentApple;
    int status = 0 ;
    sf::Clock frameRateClock;
    frameRateClock.restart();
    int frameCount = 0;
    while (window.isOpen() ) {
        /*
          shape.setPosition(x,y);
          x+=0.1;
          y+=0.1;
         */
        sf::Event event{};
        while (window.pollEvent(event)) {

            if (event.type == sf::Event::KeyPressed) {
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
                    currentSnake.trySetHeading(EAST);
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
                    currentSnake.trySetHeading(NORTH);
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
                    currentSnake.trySetHeading(WEST);
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
                    currentSnake.trySetHeading(SOUTH);
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                    window.close();
            }
            if (event.type == sf::Event::Closed)
                window.close();
        }
        ++frameCount;
        if (frameCount > 5) {
            status = currentSnake.update(currentApple);
            std::cout<<status<<std::endl;
            if (status != 0){
                std::cout << "perdu !" << std::endl;
                window.close();
            }
            frameCount = 0;
        }

        window.clear();
        currentSnake.draw(window);
        window.draw(currentApple.getShape());
        window.display();

        sf::Time timeBeforeNextFrame = sf::milliseconds(16) - frameRateClock.getElapsedTime();
        if (timeBeforeNextFrame.asMicroseconds() > 0) {
            sf::sleep(timeBeforeNextFrame);
        }

        frameRateClock.restart();

    }

    return 0;
}