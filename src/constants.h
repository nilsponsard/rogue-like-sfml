//
// Created by sautax on 05/05/2020.
//

#ifndef SFML_TEST_CONSTANTS_H
#define SFML_TEST_CONSTANTS_H

namespace snakeNameSpace{
    const int WINDOWWIDTH = 800;
    const int WINDOWHEIGHT = 600;
    const int SNAKESIZE  = 10;
}


#endif //SFML_TEST_CONSTANTS_H
