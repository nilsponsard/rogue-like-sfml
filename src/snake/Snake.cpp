//
// Created by sautax on 05/05/2020.
//
#include <iostream>
#include "../constants.h"

#include "Snake.h"

snakeNameSpace::Snake::Snake(sf::Vector2f posInitial) : head(posInitial) {
    tail.emplace_back(posInitial.x,posInitial.y - SNAKESIZE);

    for (int i = 0; i < 70; ++i) {
        grow();
    }
    heading = SOUTH;
    wantedHeading = heading;
}


int snakeNameSpace::Snake::update(snakeNameSpace::Apple &currentApple) {
    int status = 0;

    if (wantedHeading != heading) {
        switch (wantedHeading) {
            case NORTH:
                if (heading != SOUTH)
                    heading = wantedHeading;
                break;
            case EAST:
                if (heading != WEST)
                    heading = wantedHeading;
                break;
            case SOUTH:
                if (heading != NORTH)
                    heading = wantedHeading;
                break;
            case WEST:
                if (heading != EAST)
                    heading = wantedHeading;
                break;
        }
    }



    for (unsigned i = tail.size(); i > 0; --i) {
        tail[i].y = tail[i - 1].y;
        tail[i].x = tail[i - 1].x;

    }
    tail[0] = head;


    switch (heading) {

        case NORTH:
            head.y -= snakeNameSpace::SNAKESIZE;
            break;
        case EAST:
            head.x += snakeNameSpace::SNAKESIZE;
            break;
        case SOUTH:
            head.y += snakeNameSpace::SNAKESIZE;
            break;
        case WEST:
            head.x -= snakeNameSpace::SNAKESIZE;
            break;
    }

    if (currentApple.getPosition() == head) {
        grow();
        currentApple.setRandomPos();
    }
    for (int j(0);j<tail.size();++j) {

//        std::cout<< tail[j].x << " : " << tail[j].y <<std::endl;
//        std::cout<< head.x << " : " << head.y <<std::endl;

        if (head.x == tail[j].x && head.y == tail[j].y)
            status = 1; //dead
    }
    return status;
}

void snakeNameSpace::Snake::draw(sf::RenderTarget &renderTarget) {
    sf::RectangleShape headShape;
    headShape.setPosition(head);

    headShape.setFillColor(sf::Color(100, 100, 0));
    headShape.setSize(sf::Vector2f(snakeNameSpace::SNAKESIZE, snakeNameSpace::SNAKESIZE));
    renderTarget.draw(headShape);
    for (const auto &i : tail) {
        sf::RectangleShape currShape;
        currShape.setPosition(i);
        currShape.setFillColor(sf::Color(0, 255, 10));
        currShape.setSize(sf::Vector2f(snakeNameSpace::SNAKESIZE, snakeNameSpace::SNAKESIZE));
        renderTarget.draw(currShape);
//        std::cout<<currShape.getPosition().x<< " : "<<currShape.getPosition().y<<std::endl;

    }

}

void snakeNameSpace::Snake::trySetHeading(direction dir) {
    wantedHeading = dir;
}

void snakeNameSpace::Snake::grow() {

    tail.push_back(tail[0]);
}


